# Zen-Stack is about friendly plain text GTD
**Interactive Bullet Journalling in Your Terminal**
## Documentation and Help

Look on the [website](http://zen-stack.c1.biz/) and our user [Wiki](https://bitbucket.org/pan-gap/zen-stack-gtd/wiki/) for more usage examples and
tutorials. You can [reach out on Twitter](https://ctt.ac/xRNaC) for a quick question.

> Everyone _together_, all the time.  
![Zen Turtle](htdocs/docs-128.gif)


## Installation
No package for dependency managers is currently available.  
As such you'll put the extracted (n)Vim files at their destinations, as follows:

- `zen.vim` should end up in `~/.vim/ftdetect/` or similar
- `zen-rlog.vim` and `zen-todo.vim` should end up in `~/.vim/syntax/` or similar

These scripts are needed for syntax highlighting and dating things:

- `rlog-fmt` and `todo-fmt` are handy to have in your `PATH`
- `date-fmt` is required on your `PATH` for the commands to work

## Download Latest
An archive with the software can be [downloaded from here](https://bitbucket.org/pan-gap/zen-stack-gtd/downloads/).

<!-- There is also a [mirror]() for this ZIP file. -->

## Tutorial

This will teach you how to edit and filter your todo and rlog files.

FIXME

## Changes

### 2021-26 Initial release
Includes RLOG and TODO files pretty-printing with Lua,
ftdtect and syntax files for (n)Vim,
and a consise unified date formatter in Lua.


