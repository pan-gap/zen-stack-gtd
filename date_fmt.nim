# Version 2021-47 by godDLL@bitbucket.org
# Consise formatted time and date with ANSI colors

import os, times

var cmd= commandLineParams()
var
  t:DateTime
  y:DateTime
  yd:int
  d:int
  w= 1
let day= @["日", "月", "火", "水", "木", "金", "土"]

if 0 < cmd.len:
  if cmd[0] in [
                "w", "week", "l", "long", "s", "short",
                "r", "rlog", "z", "color", "x", "export"]:
    t= now()
    y= dateTime( monthday=1, month=mJan, year=t.year, hour=0, minute=0, second=0)
    d= t.weekday.int + 1
    if 7 == d:  # sunday first
        d= 0
    yd= y.weekday.int + 1
    if 7 == yd:  # sunday first
        yd= 0
    if yd in [3, 4, 5, 6]:
      w= 0
    w= w + (t.yearday + 1 + yd) div 7  # saturday increment

  case cmd[0]:
    of "c", "cycle":
      echo "Not implemented."
      system.quit()

    of "w", "week":
      echo $w & day[ d]
      system.quit()

    of "s", "short":
      echo $w & day[ d] & t.format(" HH':'mm'.'")
      system.quit()

    of "r", "rlog":
      echo $w & day[ d] & t.format(" YYYY'/'MM dd")
      system.quit()

    of "l", "long":
      echo t.format("YYYY-") & $w & day[ d] & t.format(" HH':'mm'.'")
      system.quit()

    of "t", "time":
      let t= getTime()
      echo t.format("HH':'mm") & t.format("'.'ss")
      system.quit()

    of "z", "color":
      let t= getTime()
      echo "\e[1m" & t.format("HH':'mm") & "\e[2m" & t.format("'.'ss") & "\e[m"
      system.quit()

    of "x", "export":
      echo t.format("YYYY MM dd HH':'mm '.'ss ") & $w & ' ' & day[ d] & ' ' & $(1 + t.yearday)
      system.quit()

    else:
      echo r"Usage:  date-fmt [COMMAND]"
      echo r"Like the UNIX `date` command, but not."
      echo  "Japanese days of week are used 日月火水木金土\n"
      echo r"r,rlog       week number, day name, civil date (default)"
      echo r"s,short      week number, day name, time"
      echo r"l,long       year, week number, day name, exact time"
      echo r"w,week       week number, day name"
      echo r"t,time       HH:MM.SS"
      echo r"z,color      colorized exact time"
      echo  "x,export     print everything space delimited\n"
      echo r"c,cycle      moon/sun, hours to sun rize/set in text"
      system.quit()

else:  # default  r,rlog
  t= now()
  y= dateTime( monthday=1, month=mJan, year=t.year, hour=0, minute=0, second=0)
  d= t.weekday.int + 1
  if 7 == d:  # sunday first
      d= 0
  yd= y.weekday.int + 1
  if 7 == yd:  # sunday first
      yd= 0
  if yd in [3, 4, 5, 6]:
    w= 0
  w= w + (t.yearday + 1 + yd) div 7  # saturday increment
  echo $w & day[ d] & t.format(" YYYY'/'MM dd")





