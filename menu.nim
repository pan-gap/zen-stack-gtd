import os, strscans, strutils, terminal

var cols= terminalWidth()
let item= commandLineParams()

case item[0]:
  of "-h", "--help":
    echo r"Usage:  menu [OPTIONS] ITEM1 ITEM2 ..."
    echo r"Returns selected item number, or 0."
    echo r"Arrow keys to move, Enter or Space to choose, Esc to cancel."
    echo "Works well for 2 to 20 items.\n"
    echo r"-i NUM   pre-select ITEM, defaults to 1"
    echo r"-d STR   menu item delimiter, defaults to \n"
    echo "-x       echo resulting ITEM back instead of return code\n"
    system.quit()

var
  menu:seq[string]
  index= 0
  delim= "\n"
  do_tpl= "pick"
  do_print= false
  done_parsing_args= true

for i, x in item:
  case x:
    of "-i", "--item":
      discard item[ i+1].scanf("$i", index)
      index = index - 1
      done_parsing_args= false
    of "-d", "--delim":
      delim= item[ i+1]
      done_parsing_args= false
    of "-x", "--echo":
      do_print= true
      done_parsing_args= true
    else:
      if done_parsing_args:
        menu.add x
      done_parsing_args= true

var
  result=  ""
  is_last= menu.len - 1
  go_up=   "\e[A"
  go_down= "\e[B"
  color=   "\e[7m"
  reset=   "\e[m"
  padding= " "
  go_back= "\e[0G"
  do_break= false

if "\n" == delim:
  go_back= go_back & "\e[" & $is_last & "A"

case do_tpl:
  of "pick":
    for i, x in menu:
      if index == i:
        result.add color & padding & x & padding & reset
      else:
        result.add padding & x & padding
      if i < is_last:
        result.add delim
    
    stderr.write result & go_back
    
    var got= $getch()
    if "[" == got:
      got= $getch()
      if got in ["A", "D"]:
        index= max( 0, index - 1)
      if got in ["B", "C"]:
        index= min( is_last, index + 1)
    
    while not do_break and not (got in [" ", "\r"]):
      result= ""
      for i, x in menu:
        if index == i:
          result.add color & padding & x & padding & reset
        else:
          result.add padding & x & padding
        if i < is_last:
          result.add delim
      
      stderr.write result & go_back
      got= $getch()
      if "[" == got:
        got= $getch()
        if got in ["A", "D"]:
          index= max( 0, index - 1)
        if got in ["B", "C"]:
          index= min( is_last, index + 1)


if "\n" == delim:
  stderr.write "\e[2K\e[B".repeat( is_last + 2) & go_back
else:
  stderr.write "\e[2K\e[B"

if not do_break:
  if do_print:
    echo $menu[ index]
    system.quit 1 + index
  else:
    system.quit 1 + index


