au BufRead,BufNewFile .rlog set filetype=zen-rlog
au BufRead,BufNewFile *.rlog set filetype=zen-rlog
au BufRead,BufNewFile .todo set filetype=zen-todo
au BufRead,BufNewFile *.todo set filetype=zen-todo

