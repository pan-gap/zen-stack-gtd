
syn match hash '#[^ ]*' contained
syn match base '^[^:+].*$' contains=hash
syn match owner /^[^.:]*[.:] /hs=s+1 contained
syn match done /^[+].*$/hs=s+1 contains=hash,owner
syn match sign /^[$].*$/hs=s+1 contains=hash,owner
syn match text /.*/hs=s+14 contained
syn match head /^[0-5][0-9][日月火水木金土]\s.*$/hs=s+3 contains=text,hash

hi def link hash Constant
hi def link base Function
hi def link owner Special
"hi def link done Ignore
hi def link sign Comment
hi def link text Function
hi def link head PreProc


