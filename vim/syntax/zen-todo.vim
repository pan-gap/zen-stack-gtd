
syn match term /[_a-zA-Z0-9+\- ]*[:.]/hs=s+1 contained
syn match owner /[_a-zA-Z0-9+\- ]*[:.]/ contained
syn match hash /#[_a-zA-Z0-9+\-]*/ contained
syn match mark /[0-9][0-9][日月火水木金土]\s*/ contained

syn match base '^.*$' contains=owner,hash,mark
syn match sect /^=[^:]*:*/ contained
syn match head '^=.*$' contains=sect,mark

syn match taskMark /[0-9][0-9][日月火水木金土]\s*/ contained
syn match task '^ .*$' contains=owner,hash,taskMark
syn match done '^+.*$' contains=term,hash,mark
syn match log /^%.*$/ contains=term,hash,mark

syn match note /^-.*$/hs=s+1 contains=term,hash,mark
syn match note /^>.*$/hs=s+1 contains=term,hash,mark
syn match wait /^?.*$/hs=s+1 contains=term,hash,mark
syn match wait /^\$.*$/hs=s+1 contains=term,hash,mark

syn match date /^[0-9][0-9][日月火水木金土]\s*.*/ contains=mark,hash
syn match time /[0-2][0-9]:[0-5][0-9]/ contained
syn match mark /^[0-9][0-9][日月火水木金土]\s*[0-2][0-9]:[0-5][0-9]/ contains=time,hash

hi def link sect Todo
hi def link term Constant
hi def link hash Constant
hi def link done PreProc
hi def link log PreProc
hi def link note Function
hi def link wait Comment
hi def link mark Special
hi def link taskMark Special
hi def link time Macro
hi def link head Special
hi def link task Label
hi task ctermfg=Blue ctermbg=Black
hi owner ctermfg=94
hi term ctermfg=94
hi hash ctermfg=Green ctermbg=Black
hi taskMark ctermfg=White ctermbg=Black term=bold cterm=bold
hi done ctermfg=90
hi log ctermfg=104
hi time ctermfg=90
hi sect ctermfg=195 ctermbg=17 term=bold cterm=bold

syn region sect start=/^=/ end=/^$/ fold transparent
set foldlevel=1
set foldmethod=syntax


